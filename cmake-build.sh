#!/bin/bash

cmake \
  -S ./ \
  -B ./build/ \
  -DBOOST_VERSION=1.74.0 \
  -G "Unix Makefiles" && \
make \
  -C ./build/ \
  -j $(nproc);

#cmake \
#  --build ./build
